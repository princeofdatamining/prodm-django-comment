from django.db import models
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.conf import settings
from django.utils import timezone

from jsonfield.fields import JSONField

from . import constant

USER = settings.AUTH_USER_MODEL

class CommentManager(models.Manager):

    def public(self, *args, queryset=None, **kwargs):
        return (self if queryset is None else queryset).filter(*args, visible=True, **kwargs)

Comments = CommentManager()

class Comment(models.Model):

    class Meta:
        verbose_name = constant.MODEL_COMMENT
        verbose_name_plural = constant.MODEL_COMMENTS
        ordering = ['-visible', '-create_time']

    objects = Comments

    user = models.ForeignKey(USER, verbose_name=constant.USER)
    create_time = models.DateTimeField(constant.TIME_CREATE, null=True, blank=True, default=timezone.now)
    title = models.CharField(constant.TITLE, blank=True, default='', max_length=191)
    content = JSONField(constant.CONTENT)
    content_type = models.ForeignKey(ContentType)
    object_id = models.CharField(max_length=191)
    related = GenericForeignKey()
    category = models.CharField(constant.CATEGORY, blank=True, default='', max_length=32)
    visible = models.BooleanField(constant.VISIBLE, default=True)

    def __str__(self):
        return self.title or str(self.user)
