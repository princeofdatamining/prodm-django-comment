from rest_framework import validators, status
from rest_framework.generics import *
from rest_framework.permissions import *
from rest_framework.serializers import *
from rest_framework.exceptions import *
from rest_framework.response import Response

from qc_utils.rest_framework.views import *
from qc_utils.rest_framework.permissions import *
from qc_utils.rest_framework.serializers import *
from qc_utils.rest_framework.fields import *
from qc_utils.rest_framework.utils import *

from .. import models, constant

registry = {
    # 'scene': {
    #     'category': '',
    #     'content_type_id': qc_utils.django.modelutils.get_content_type_id(XXX),
    #     'queryset': XXX.objects.filter(...),
    # },
}

class SubmitSerializer(ModelSerializer):

    # 自动设置作者
    USER_FIELDS = 'user_id'
    content = ContentJsonField()

    class Meta:
        model = models.Comment
        fields = ('content',)

    def save(self, **kwargs):
        view = self.context['view']
        # 自动写入 关联对象
        kwargs.update(dict(
            content_type_id=view.content_type_id,
            object_id=view.object_id,
            category=view.category,
        ))
        return super(SubmitSerializer, self).save(**kwargs)

class Serializer(ModelSerializer):

    url = HyperlinkedIdentityField('comment')

    class Meta:
        model = models.Comment
        fields = ('id', 'url',
            'user', 'content',
            'create_time', 'title', 'visible',
        )

class CommentsView(ListCreateAPIView):

    def initial(self, request, *args, **kwargs):
        super(CommentsView, self).initial(request, *args, **kwargs)
        # 从 path 获取参数
        self.scene, self.object_id = self.kwargs.get('scene'), self.kwargs.get('object_id')
        # 检查是否注册
        settings = registry.get(self.scene)
        if not settings:
            raise PermissionDenied
        self.category = settings.get('category') or ''
        self.content_type_id = settings.get('content_type_id')
        self.related_queryset = settings.get('queryset')
        # 用 queryset 去检查 object_id 的权限
        try:
            self.related = self.related_queryset.get(pk=self.object_id)
        except:
            raise Http404
            self.related = None

    permission_classes = (IsAuthenticatedOrReadOnly,)
    queryset = models.Comments.all()
    submit_serializer_class = SubmitSerializer
    serializer_class = Serializer
    def get_queryset(self):
        qs = super(CommentsView, self).get_queryset()
        if self.related is None:
            return qs.none()
        return qs.filter(
            content_type_id=self.content_type_id, 
            object_id=self.object_id,
            category=self.category,
        )

class CommentView(RetrieveAPIView):

    queryset = models.Comments.public()
    serializer_class = Serializer

#

from django.conf.urls import url, include
urlpatterns = [
    url(r'^(?P<scene>[^\/]+)/(?P<object_id>[^\/]+)/$', CommentsView.as_view(), name='comments'),
    url(r'^(?P<pk>\d+)$', CommentView.as_view(), name='comment'),
    #
]
from rest_framework.urlpatterns import format_suffix_patterns
urlpatterns = format_suffix_patterns(urlpatterns)
