from qc_utils.rest_framework.tests.base import APITestCase
from rest_framework.reverse import reverse

from qc_utils.django.modelutils import get_content_type_id
from rest_auth.models import Users

from json_comment import models
from json_comment.restful.v1 import registry

class TestComment(APITestCase):

    def setUp(self):
        super(TestComment, self).setUp()
        self.url = self.reverse_api('comments', kwargs=dict(scene='$scene$', object_id='$object$'))
        self.u01 = self.force_user('user01', password='1')
        self.u02 = self.force_user('user02', password='1', is_staff=True)
        self.u03 = self.force_user('user03', password='1')
        self.user_ctid = get_content_type_id(Users.model)
        registry.update({
            'user': {
                'content_type_id': self.user_ctid,
                'queryset': Users.filter(is_superuser=False, is_staff=False),
            },
            'staff': {
                'content_type_id': self.user_ctid,
                'queryset': Users.filter(is_superuser=False, is_staff=True),
                'category': 'staff',
            },
        })

    def make_url(self, scene, object_id):
        return self.url.replace('$scene$', scene).replace('$object$', str(object_id))

    def test_unregister(self):
        url = self.make_url('blah', 'blah')
        resp = self.client.get(url)
        self.assertEqual(403, resp.status_code)

    def test_cant_filter(self):
        url = self.make_url('user', self.u02.pk)
        resp = self.client.get(url)
        self.assertEqual(404, resp.status_code)

    def test_cant_post(self):
        url = self.make_url('user', self.u01.pk)
        resp = self.client.post(url)
        self.assertEqual(401, resp.status_code)

    def test_category(self):
        url = self.make_url('user', self.u01.pk)
        resp = self.client.post(url, {
            'content': [{"value": ""}],
        }, HTTP_AUTHORIZATION='token '+self.u03.token)
        self.assertEqual(201, resp.status_code)
        self.check_comment(resp.data['id'], '', self.u01)
        #
        url = self.make_url('staff', self.u02.pk)
        resp = self.client.post(url, {
            'content': [{"value": ""}],
        }, HTTP_AUTHORIZATION='token '+self.u03.token)
        self.assertEqual(201, resp.status_code)
        self.check_comment(resp.data['id'], 'staff', self.u02)

    def check_comment(self, pk, category, related):
        comment = models.Comments.get(pk=pk)
        self.assertEqual(self.u03.pk, comment.user_id)
        self.assertEqual(category, comment.category)
        self.assertEqual(self.user_ctid, comment.content_type_id)
        self.assertEqual(str(related.pk), comment.object_id)
