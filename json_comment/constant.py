from django.utils.translation import ugettext_lazy as _, ungettext_lazy, pgettext_lazy, npgettext_lazy

APP_VERBOSE_NAME = pgettext_lazy('APP', 'json_comment')

MODEL_COMMENT = pgettext_lazy('Model', 'Comment')
MODEL_COMMENTS = pgettext_lazy('Model', 'Comments')

USER = _('User')
CATEGORY = pgettext_lazy('Comment', 'category')
TITLE = pgettext_lazy('Comment', 'title')
CONTENT = pgettext_lazy('Comment', 'content')
TIME_CREATE = pgettext_lazy('Comment', 'create time')
VISIBLE = pgettext_lazy('Comment', 'visible')
