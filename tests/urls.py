from django.conf.urls import url, include
urlpatterns = (
    url(r'^api/auth/', include('rest_auth.restful.v1')),
    url(r'^api/comments/', include('json_comment.restful.v1')),
)
