from django.apps import AppConfig, apps

from rest_auth import models

class CoreAppConfig(AppConfig):

    name = 'core'

    def ready(self):
        models.setup_users()
        try:
            self.build_data()
        except:
            pass

    def build_data(self):
        from django.contrib.auth.models import User
        root, created = User.objects.get_or_create(defaults=dict(
            is_superuser=True, is_staff=True, is_active=True,
        ), username='root')
        if created:
            root.set_password('password')
            root.save()
        #
        from django.contrib.contenttypes.models import ContentType
        from json_comment.restful.v1 import registry
        registry['user'] = dict(
            content_type_id=ContentType.objects.get_for_model(User).pk,
            queryset=User.objects.all(),
        )
        #
